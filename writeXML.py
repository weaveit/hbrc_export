

# How to Use the Python Requests Module With REST APIs | Nylas
# https://www.nylas.com/blog/use-python-requests-module-rest-apis/

# Python and REST APIs: Interacting With Web Services – Real Python
# https://realpython.com/api-integration-in-python/

#Python ftp
#https://stackoverflow.com/questions/5663787/upload-folders-from-local-system-to-ftp-using-python-script/39994026

#ftp test server
#https://phoenixnap.com/kb/install-ftp-server-on-ubuntu-vsftpd
#https://www.linuxquestions.org/questions/linux-server-73/vsftpd-error-failed-to-retrieve-directory-listing-878838/

# pip install lxml


print("Here we go!")
#!/usr/bin/python
#import xml.etree.ElementTree as ET
from lxml import etree as ET

root = ET.Element("Hilltop")
ET.SubElement(root, "Agency").text = "FarmSense"
# can be looped here to add multiple sites to one file
measurement = ET.SubElement(root, "Measurement", SiteName="123456M1")
datasource = ET.SubElement(measurement, "DataSource",Name="Water Meter", NumItems="1") # Name="Compliance Volume"   Name="Meter Reading"   Name="Abstraction Volume"
ET.SubElement(datasource, "Interpolation").text = "Incremental"
ET.SubElement(datasource, "DataType").text = "MeterReading"

data = ET.SubElement(measurement, "Data",
                     DateFormat="Calendar")  # DateFormat="UTC"
ET.SubElement(data, "V").text = "10-Apr-2008 11:06:15 10"  # 24hr time
ET.SubElement(data, "V").text = "10-Apr-2008 11:25:56 10"
ET.SubElement(data, "V").text = "10-Apr-2008 11:46:02 10"
ET.SubElement(data, "V").text = "10-Apr-2008 12:05:15 10"

tree = ET.ElementTree(root)
tree.write("filename.xml", pretty_print=True, xml_declaration="true")

#https://stackoverflow.com/questions/19351127/python-xml-etree-elementtree-directory-acess

print("XML Done!")
print("Starting FTP...")

import ftplib
import os

server = ''
username = ''
password = ''
myFTP = ftplib.FTP(server, username, password)
myPath = r'c:\ftp_test'


def uploadThis(path):
    files = os.listdir(path)
    print (files)
    os.chdir(path)
    for f in files:
        if os.path.isfile(path + r'\{}'.format(f)): #if os.path.isfile(path + r'\{}'.format(f)):
            fileToSend = open(f, 'rb')
            myFTP.storbinary('STOR %s' % f, fileToSend)
            #myFTP.storbinary('STOR filename.xml', fileToSend)
            fileToSend.close()
        elif os.path.isdir(path + r'\{}'.format(f)):
            myFTP.mkd(f)
            myFTP.cwd(f)
            uploadThis(path + r'\{}'.format(f))
    myFTP.cwd('..')
    os.chdir('..')


uploadThis(myPath) # now call the recursive function 