import logging
import json
import time
# Importing models and REST client class from Professional Edition version
from tb_rest_client.rest_client_pe import *
from tb_rest_client.rest import ApiException
from tb_rest_client.rest_client_base import *

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(levelname)s - %(module)s - %(lineno)d - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

# ThingsBoard REST API URL
url = "http://portal.farmsense.nz"

# Default Tenant Administrator credentials
username = ""
password = ""

# Config for HBRC 
attribute_key='HBRCWaterMeterSiteName'
telemetry_key ="WaterMeter"
entity_type = "DEVICE"
device_type = "Sensor Hub"
device_id = "fd9245e0-41c4-11ec-80d5-2d2bcb29f331" 

#entity_hbrc_id='34888f90-27b9-11eb-8d17-c9101bea1ef6'
#entity_hbrc_key='B'



startTs = 1637700200000#int(time())
endTs = 1637790156000 #int(time())
print ("Requesting data for this timerange: {0} - {1}".format(startTs, endTs))

# Creating the REST client object with context manager to get auto token refresh
with RestClientPE(base_url=url) as rest_client:
    try:
        rest_client.login(username=username, password=password)

        parameters = {}
        parameters["entityId"] = {}
        parameters["entityId"]["entityType"] = entity_type
        parameters["rootId"] = device_id
        parameters["rootType"] = "ENTITY_VIEW"
        parameters["direction"] = "TO"
        parameters["relationTypeGroup"] = "COMMON"
        parameters["maxLevel"] = 1
        parameters["fetchLastLevelOnly"] = True

        device_types = []
        device_types.append(device_type)

        device_query = DeviceSearchQuery(device_types, parameters)
            
        devices = rest_client.find_by_query_v1(body=device_query)
        
        for device in devices:
            device_entity_type = device.id.entity_type
            device_entity_id = device.id.id
            
            attributes = rest_client.get_attributes(entity_type=device_entity_type, entity_id=device_entity_id)
            print ("\nFound device", device.name,". Searching for attriutes starting with",attribute_key,". Device id: ",device_entity_id)
            for attribute in attributes:
                
                if ('key' in attribute ) & ( attribute["key"].find(attribute_key) == 0 ):
                    telemetry_key_port = str(telemetry_key) + attribute["key"].replace(attribute_key, "")
                    print ("\nFound ", attribute["key"], " with value: ", attribute["value"], ". Searching now for telementy for: ", telemetry_key_port)

                    timeseries = rest_client.get_timeseries(entity_type=device_entity_type, start_ts=startTs, end_ts=endTs, entity_id=device_entity_id, keys=telemetry_key_port)
                    for telementy in telementy[telemetry_key_port]:
                        print ("time: ", telementy["ts"], "value", telementy['value'])
            
                    

    except ApiException as e:
        exception(e)















    
