from datetime import datetime, date, timedelta

from lxml import etree as ET

# Importing models and REST client class from Professional Edition version
from tb_rest_client.rest_client_pe import *
from tb_rest_client.rest import ApiException
from tb_rest_client.rest_client_base import *

debug_logging_enabled = True

# ThingsBoard REST API URL
url = "http://portal.farmsense.nz"

# Default Tenant Administrator credentials
username = ""
password = ""

outboxDirectory = "C:\\Users\\OEM\\Desktop\\ThingsBoard\\"

# Output settings
output_root_name = "Hilltop"
xml_date_format = "Calendar"

# Config for HBRC
agency_name = "FarmSense"
sitename_key="HBRCWaterMeter"
attribute_key='HBRCWaterMeterSiteName'
telemetry_key ="WaterMeter"
telemetry_xml_name = "Water Meter"

entity_type = "DEVICE"
device_type = "Sensor Hub"
entity_filter_id = "fd9245e0-41c4-11ec-80d5-2d2bcb29f331"

site_name = "SiteName"

today = datetime.utcnow().date()
yesterday = today - timedelta(days=1)

startTs = int(datetime.combine(yesterday, datetime.min.time()).timestamp()*1000)
endTs =  int(datetime.combine(yesterday, datetime.max.time()).timestamp()*1000)

# Working test data timestamps for debugging 
#startTs = 1637790071000
#endTs = 1637790079000

dateformat = "%d-%b-%Y"
timeformat = dateformat +" %H:%M:%S"
startDate = datetime.fromtimestamp(startTs / 1e3)
date =  startDate.strftime(dateformat)
dateFrom = startDate.strftime(timeformat)
dateTo = datetime.fromtimestamp(endTs / 1e3).strftime(timeformat)

def get_devices(rest_client, root_id):
    parameters = {
        "entityId" : {
            "entityType" : entity_type
        }
        ,"rootId" : root_id
        ,"rootType" : "ENTITY_VIEW"
        ,"direction" : "TO"
        ,"relationTypeGroup" : "COMMON"
        ,"maxLevel" : 1
        ,"fetchLastLevelOnly" : True
    }
    return rest_client.find_by_query_v1(body=DeviceSearchQuery([device_type], parameters))

def is_attribute_key(attribute, key):
    return ('key' in attribute ) & ( attribute["key"].find(key) == 0 )
    
def get_an_attribute(attributes, key):
    for attribute in attributes:
        if is_attribute_key(attribute, key):
            return attribute

def get_telemetry_attribute(attributes):
    return get_an_attribute(attributes, attribute_key)

def get_sitename_attribute(attributes):
    return get_an_attribute(attributes, sitename_key)

def get_telemetry_value(attribute):
    return str(telemetry_key) + attribute["key"].replace(attribute_key, "")

def get_time_formatted(date_object):
    return datetime.fromtimestamp(date_object / 1e3).strftime(timeformat)
    
def get_output_filepath(attribute):
    return  outboxDirectory + attribute["value"] + "_" + str(date) + ".xml"

def get_formatted_telemetry_data(telementy_data):
    return f"{get_time_formatted(telementy_data['ts'])} {telementy_data['value']}"

def generate_xml_object():
    root = ET.Element(output_root_name)
    ET.SubElement(root, "Agency").text = agency_name
    return root

def add_telementy_xml(root, name, attribute):
    measurement = ET.SubElement(root, "Measurement", SiteName=attribute["value"])
    datasource = ET.SubElement(measurement, "DataSource",Name=name, NumItems="1") 
    ET.SubElement(datasource, "Interpolation").text = "Incremental"
    ET.SubElement(datasource, "DataType").text = "MeterReading"
    return ET.SubElement(measurement, "Data", DateFormat=xml_date_format)  

def write_xml_to_file(root, filepath):
    tree = ET.ElementTree(root)
    tree.write(filepath, pretty_print=True, xml_declaration="true")     

def print_debug(text):
    if debug_logging_enabled:
        print (text)

with RestClientPE(base_url=url) as rest_client:
    try:
        rest_client.login(username=username, password=password)
        print_debug (f"0. Logged in as {username}")

        devices = get_devices(rest_client, entity_filter_id)
        print_debug (f"\n1. Found {len(devices)} devices")

        for device in devices:
            print_debug (f"\n2. Found device {device.name}")
            
            device_entity_type = device.id.entity_type
            device_entity_id = device.id.id
            
            attributes = rest_client.get_attributes(
                entity_type=device_entity_type, 
                entity_id=device_entity_id
            )
            print_debug (F"\n3. Checking if device has {attribute_key} and {sitename_key} values")

            telemetry_attribute = get_telemetry_attribute(attributes)
            sitename_attribute = get_sitename_attribute(attributes)
            if telemetry_attribute is None or sitename_attribute is None:
                print_debug (f"3.0. This devices doesn't required data. Telementy {telemetry_attribute} found. Sitename {sitename_attribute} found. ")
                continue
            else:
                filepath = get_output_filepath(sitename_attribute)
                print_debug (f"3.1. Sitename data found, filepath to use: {filepath}")
                    
            telemetry_key_port = get_telemetry_value(telemetry_attribute)
            print_debug (f"3.2. Found {telemetry_attribute['key']} with value: {telemetry_attribute['value']}.")

            print_debug (f"\n4. Requesting data for {telemetry_key_port} in timerange: {dateFrom} - {dateTo}")
            timeseries = rest_client.get_timeseries(
                entity_type=device_entity_type
                , start_ts=startTs
                , end_ts=endTs
                , entity_id=device_entity_id
                , keys=telemetry_key_port)

            if telemetry_key_port not in timeseries:    
                print_debug (f"4.0. key: '{telemetry_key_port}' not in timeseries")
            else:
                telementy_results = timeseries[telemetry_key_port]
                print_debug (f"4.1. key: '{telemetry_key_port}' found, trying to read data")

                root = generate_xml_object()
                telementy_xml_object = add_telementy_xml(root, telemetry_xml_name, telemetry_attribute) 

                for telementy_data in telementy_results:
                    ET.SubElement(telementy_xml_object, "V").text = get_formatted_telemetry_data(telementy_data)

                print_debug (f"\n5. Data retrieved - Trying to save to: '{filepath}'")
                write_xml_to_file(root, filepath)           
                print_debug (f"\n6. Data written to: '{filepath}'")

    except ApiException as e:
        print_debug (f"\nError occurred")
        print(e)

print_debug (f"\nFinished running script.")  