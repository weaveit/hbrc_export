


#https://thingsboard.io/docs/pe/reference/python-rest-client/#professional-edition-python-rest-client-example

#pip3 install tb-rest-client

import logging
from json import load
from tb_rest_client import rest_client_pe
# Importing models and REST client class from Professional Edition version
from tb_rest_client.rest_client_pe import *
from tb_rest_client.rest import ApiException
import json

print("Here we go!")

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(levelname)s - %(module)s - %(lineno)d - %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S')

# ThingsBoard REST API URL
url = "http://portal.farmsense.nz"

# Default Tenant Administrator credentials
username = ""
password = ""



# Creating the REST client object with context manager to get auto token refresh
with RestClientPE(base_url=url) as rest_client:
    try:
        # Auth with credentials
        rest_client.login(username=username, password=password)
        #rest_client.token_info()
        #rest_client.token_info()
        #t = rest_client.get_token()
        #print("### Token: ",t)
        # Getting current user
        #current_user = rest_client.get_user()
        #print("### Current User: ", current_user, "  ### END")

        #customers = rest_client.get_customers(page_size=5, page=1)
        response = rest_client.get_user_devices(type='Sensor Hub', page_size=1, page=1)
        #jsonResponse = json.loads(response)

        #rest_client.dev
        print("### get_user_devices: ")
        print(response)
        print("  ### END")

        response = rest_client.get_timeseries(entity_type='DEVICE', start_ts='1633885131000', end_ts='1633899531000', entity_id='34888f90-27b9-11eb-8d17-c9101bea1ef6',keys='B')
        print("### response: ")
        print(response)
        print("  ### END")
        #jsonResponse = json.loads(response)
        print("  #################### END")
        #for i in response:
            #print (response['B']['ts'])
        print (response['B']) #this works!!!
        
            #print (response.B)

        print("  #################### END")

        #for j in response['B']:
         #   print (response['B']['ts'])

        for key, value in response.items():
            print("Key:")
            print(key)

        print("  #################### END")
        for key, value in response.items():
            print (key, value)
            #print(key)

        print("  #################### END")
        #d = {}
        #for row in response.get('B'):
            #d[row[0]] = row[1]
            #print (row[row], value)
            #print(key)

         #ts_values =    

    except ApiException as e:
        logging.exception(e)
